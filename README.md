# stldevs
St. Louis developer profiles (http://stldevs.com)

[![Build Status](https://secure.travis-ci.org/jakecoffman/stldevs.png?branch=master)](http://travis-ci.org/jakecoffman/stldevs)

## motivations

- St. Louis designers and developers that contribute to open source should be more visible
- St. Louis designers and developers that are interested in certain languages and technologies need an easier way to find eachother
- St. Louis companies need ways to view designers and developers without going through recruiters that already have systems to scrape github et al
- fun

## contributing

Pull requests are welcome as long as you don't take the site in a new weird direction. All Go files should be run through gofmt and goimport before pushing. I suggest GoSublime or IntelliJ Go plugin (built from source).

Front end developers looks here: https://github.com/jakecoffman/stldevs-frontend
